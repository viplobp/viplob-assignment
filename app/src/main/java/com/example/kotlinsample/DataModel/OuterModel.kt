package com.example.kotlinsample.DataModel

import com.example.kotlinsample.DataModel.Model

data class OuterModel(val title: String, val modelList: ArrayList<Model>)
{

}
