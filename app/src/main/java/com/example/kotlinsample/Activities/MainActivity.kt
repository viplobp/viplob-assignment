package com.example.kotlinsample.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinsample.Adapter.InnerListAdapter
import com.example.kotlinsample.Adapter.OuterListAdapter
import com.example.kotlinsample.DataModel.Model
import com.example.kotlinsample.DataModel.OuterModel
import com.example.kotlinsample.R

class MainActivity : AppCompatActivity() {

    val innerList = ArrayList<Model>()
    val outerList = ArrayList<OuterModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createInnerList()
    }

    fun createInnerList() {
        val model1 = Model(
            "https://i.picsum.photos/id/10/2500/1667.jpg?hmac=J04WWC_ebchx3WwzbM-Z4_KC_LeLBWr5LZMaAkWkF68",
            "https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U",
            ""
        )
        innerList.add(model1)
        val model2 = Model(
            "",
            "",
            "https://i.picsum.photos/id/1001/5616/3744.jpg?hmac=38lkvX7tHXmlNbI0HzZbtkJ6_wpWyqvkX4Ty6vYElZE"
        )
        innerList.add(model2)
        val model3 = Model(
            "https://i.picsum.photos/id/1015/6000/4000.jpg?hmac=aHjb0fRa1t14DTIEBcoC12c5rAXOSwnVlaA5ujxPQ0I",
            "https://i.picsum.photos/id/1016/3844/2563.jpg?hmac=WEryKFRvTdeae2aUrY-DHscSmZuyYI9jd_-p94stBvc",
            ""
        )
        innerList.add(model3)
        val model4 = Model(
            "",
            "",
            "https://i.picsum.photos/id/1003/1181/1772.jpg?hmac=oN9fHMXiqe9Zq2RM6XT-RVZkojgPnECWwyEF1RvvTZk"
        )
        innerList.add(model4)

        val model5 = Model(
            "https://i.picsum.photos/id/1003/1181/1772.jpg?hmac=oN9fHMXiqe9Zq2RM6XT-RVZkojgPnECWwyEF1RvvTZk",
            "https://i.picsum.photos/id/1016/3844/2563.jpg?hmac=WEryKFRvTdeae2aUrY-DHscSmZuyYI9jd_-p94stBvc",
            ""
        )
        innerList.add(model5)
        val model6 = Model(
            "",
            "",
            "https://i.picsum.photos/id/1001/5616/3744.jpg?hmac=38lkvX7tHXmlNbI0HzZbtkJ6_wpWyqvkX4Ty6vYElZE"
        )
        innerList.add(model6)
        createOuterList()
    }

    fun createOuterList()
    {
        val model1 = OuterModel(
            "Section1",
            innerList
        )
        outerList.add(model1)
        val model2 = OuterModel(
            "Section2",
            innerList
        )
        outerList.add(model2)
        val model3 = OuterModel(
            "Section3",
            innerList
        )
        outerList.add(model3)
        val model4 = OuterModel(
            "Section4",
            innerList
        )
        outerList.add(model4)
        val model5 = OuterModel(
            "Section5",
            innerList
        )
        outerList.add(model5)
        val model6 = OuterModel(
            "Section6",
            innerList
        )
        outerList.add(model6)
        val model7 = OuterModel(
            "Section7",
            innerList
        )
        outerList.add(model7)
        val model8 = OuterModel(
            "Section8",
            innerList
        )
        outerList.add(model8)
        val model9 = OuterModel(
            "Section9",
            innerList
        )
        outerList.add(model9)
        val model10 = OuterModel(
            "Section10",
            innerList
        )
        outerList.add(model10)
        showList()
    }

    fun showList()
    {
        var recylerview = findViewById<RecyclerView>(R.id.recylerview)
        val adapter = OuterListAdapter(this@MainActivity, outerList)
        recylerview.setNestedScrollingEnabled(false);
        recylerview.setHasFixedSize(false);
        recylerview.layoutManager = LinearLayoutManager(this@MainActivity)
        recylerview.adapter = adapter
    }



}