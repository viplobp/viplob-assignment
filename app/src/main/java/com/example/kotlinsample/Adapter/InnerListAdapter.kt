package com.example.kotlinsample.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlinsample.DataModel.Model
import com.example.kotlinsample.R


class InnerListAdapter(private val context: Context, private val list: ArrayList<Model>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        internal var image1: ImageView
        internal var image2: ImageView
        internal var image3: ImageView
        internal var layout1: LinearLayout


        init {
            layout1 = itemView.findViewById(R.id.layout1)
            image1 = itemView.findViewById(R.id.image1)
            image2 = itemView.findViewById(R.id.image2)
            image3 = itemView.findViewById(R.id.image3)
        }

        internal fun bind(position: Int) {

            if (list[position].url1.length == 0) {
                image1.visibility = View.GONE
                image2.visibility = View.GONE
                layout1.visibility = View.GONE
                image3.visibility = View.VISIBLE
                Glide.with(context)
                    .load(list[position].url3)
                    .into(image3)
            } else {
                layout1.visibility = View.VISIBLE
                image1.visibility = View.VISIBLE
                image2.visibility = View.VISIBLE
                image3.visibility = View.GONE

                Glide.with(context)
                    .load(list[position].url1)
                    .into(image1)
                Glide.with(context)
                    .load(list[position].url2)
                    .into(image2)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.inner_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}