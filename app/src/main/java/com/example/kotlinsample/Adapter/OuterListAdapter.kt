package com.example.kotlinsample.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.example.kotlinsample.DataModel.OuterModel
import com.example.kotlinsample.R


class OuterListAdapter(private val context: Context, private val list: ArrayList<OuterModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        internal var title: TextView
        internal var recylerview: RecyclerView


        init {
            title = itemView.findViewById(R.id.title)
            recylerview = itemView.findViewById(R.id.recylerview)

        }
        internal fun bind(position: Int) {
            val mScrollTouchListener: OnItemTouchListener = object : OnItemTouchListener {
                override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                    val action = e.action
                    when (action) {
                        MotionEvent.ACTION_MOVE -> rv.parent.requestDisallowInterceptTouchEvent(true)
                    }
                    return false
                }

                override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
                override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}
            }

            recylerview.addOnItemTouchListener(mScrollTouchListener)
            val adapter = InnerListAdapter(context, list[position].modelList)
            recylerview.layoutManager = LinearLayoutManager(context)
            recylerview.adapter = adapter

            title.text = list[position].title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.outer_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}